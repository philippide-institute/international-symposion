<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#" 
    xmlns:swrc="http://swrc.ontoware.org/ontology#" 
    xmlns:dc="http://purl.org/dc/terms/" 
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
    exclude-result-prefixes="rdf swrc dc rdfs" version="1.0">
    <xsl:output method="text" encoding="UTF-8" />
    <xsl:param name="parent_dir" />
    <xsl:template match="/">
        <xsl:variable name="volume-url" select="/rdf:RDF/swrc:Conference/rdfs:seeAlso/@rdf:resource"/>
        <html>
            <xsl:document href="{$parent_dir}/metadata.html">
                <article xmlns="http://www.w3.org/1999/xhtml">
                    <div>
                        <xsl:value-of select="/rdf:RDF/swrc:Conference/swrc:description[2]"></xsl:value-of>
                    </div>
                    <div>
                        <a href="{$volume-url}" target="_blank">volum</a>
                    </div>
                </article>
            </xsl:document>
        </html>         
    </xsl:template>
</xsl:stylesheet>

<!-- xsltproc ./modules/data-to-html-and-index-records/volume-metadata-to-html.xsl ./data/2002/metadata.xml -->